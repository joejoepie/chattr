using System;

namespace Chattr
{
    public enum EventType
    {
        ConsoleResized
    }

    public class ConsoleEvent
    {
        public EventType Type {get;}

        public ConsoleEvent(EventType type)
        {
            Type = type;
        }
    }
}