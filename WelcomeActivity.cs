using System;
using System.Threading;
using System.Threading.Tasks;

namespace Chattr
{
    public class WelcomeActivity : ConsoleActivity
    {
        private Chattr root;
        private string username = "";
        private string serverUrl = "";
        private const string enterUsernameMessage = "Username: ";
        private const string enterUrlMessage = "Server URL: ";

        public WelcomeActivity(Chattr chattr)
        {
            root = chattr;
        }
        private void HandleInput()
        {

        }
        public override void Activate()
        {
            Draw();
            Read();
            ChatroomActivity a = new ChatroomActivity(username, serverUrl);
            root.SwitchActivity(a);
            a.Activate();
        }
        private void Read()
        {
            Console.SetCursorPosition(Console.WindowWidth/2 + 1, Console.WindowHeight/2 - 1);
            username = Console.ReadLine();
            //Draw();

            Console.SetCursorPosition(Console.WindowWidth/2 + 1, Console.WindowHeight/2 + 1);
            serverUrl = Console.ReadLine();
        }
        public override void Draw()
        {
            Console.Clear();

            Console.ForegroundColor = ConsoleColor.White;
            Console.SetCursorPosition(0, 0);
            // Draw top line
            for (int x = 0; x < Console.WindowWidth; x++)
            {
                Console.Write("#");
            }
            Console.SetCursorPosition(0, 4);
            // Draw bottom line
            for (int x = 0; x < Console.WindowWidth; x++)
            {
                Console.Write("#");
            }

            Console.SetCursorPosition(0, 1);Console.Write("#");
            Console.SetCursorPosition(0, 2);Console.Write("#");
            Console.SetCursorPosition(0, 3);Console.Write("#");
            Console.SetCursorPosition(Console.WindowWidth-1, 1);Console.Write("#");
            Console.SetCursorPosition(Console.WindowWidth-1, 2);Console.Write("#");
            Console.SetCursorPosition(Console.WindowWidth-1, 3);Console.Write("#");

            Console.ForegroundColor = ConsoleColor.Black;
            ConsoleHelperFunctions.WriteCenter("Welcome to Chattr!", 2);

            Console.SetCursorPosition(Console.WindowWidth/2 - enterUsernameMessage.Length, Console.WindowHeight/2 - 1);
            Console.Write(enterUsernameMessage + username);

            Console.SetCursorPosition(Console.WindowWidth/2 - enterUrlMessage.Length, Console.WindowHeight/2 + 1);
            Console.Write(enterUrlMessage + serverUrl);

        }
        public override void HandleEvent(ConsoleEvent e)
        {
            switch(e.Type)
            {
                case EventType.ConsoleResized:
                    Draw();
                    if (username == "")
                    {
                        Console.SetCursorPosition(Console.WindowWidth/2 + 1, Console.WindowHeight/2 - 1);
                    }
                    else
                    {
                        Console.SetCursorPosition(Console.WindowWidth/2 + 1, Console.WindowHeight/2 + 1);
                    }
                    break;
            }
        }
    }
}