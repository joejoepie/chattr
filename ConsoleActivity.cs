using System;

namespace Chattr
{
    public abstract class ConsoleActivity
    {
        public abstract void Activate();
        public abstract void Draw();
        public abstract void HandleEvent(ConsoleEvent e);
    }
}