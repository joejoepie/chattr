using System;
using System.Threading;
using System.Net;
using System.Net.Sockets;

namespace Chattr
{
    public class Chattr : IConsoleEventHandler
    {
        public static readonly ConsoleColor BACKGROUND_COLOR = ConsoleColor.DarkCyan;
        public string Username {get;}
        public string ServerUrl {get;}
        private ConsoleEventListener eventListener;

        // If i don't make the variable this way, i can't change it in member functions
        // of this class... Weird
        private ConsoleActivity currentActivity;
        public ConsoleActivity CurrentActivity 
        {
            get
            {
                return currentActivity;
            }
        }

        public Chattr()
        {
            SetupConsole();
            WelcomeActivity welcome = new WelcomeActivity(this);
            SwitchActivity(welcome);
            welcome.Activate();
            eventListener.Stop();
        }
        private void SetupConsole()
        {
            Console.BackgroundColor = ConsoleColor.DarkCyan;
            Console.ForegroundColor = ConsoleColor.Black;

            Console.Clear();

            eventListener =  new ConsoleEventListener(this);
            eventListener.Listen();
        }
        public void HandleEvent(ConsoleEvent consoleEvent)
        {
            if (Environment.OSVersion.Platform == PlatformID.Win32NT)
            {
                try
                {
                    Console.SetBufferSize(Console.WindowWidth, Console.WindowHeight);
                    Console.MoveBufferArea(0, 0, Console.WindowWidth, Console.WindowHeight, Console.WindowLeft, Console.WindowTop);
                }
                catch(Exception e)
                {
                    Console.WriteLine("Window width, height: " + Console.WindowWidth + ", " + Console.WindowHeight);
                    Console.WriteLine("Largest possible window width, height: " + Console.LargestWindowWidth + ", " + Console.LargestWindowHeight);
                    Console.WriteLine("Error: " + e.Message);
                }
            }
            currentActivity.HandleEvent(consoleEvent);
        }
        public void SwitchActivity(ConsoleActivity activity)
        {
            currentActivity = activity;
        }
    }
}