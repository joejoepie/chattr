using System;
using System.Collections.Generic;
using System.Threading;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

namespace Chattr
{
    public class ChatroomActivity : ConsoleActivity
    {
        public string Username {get; set;}
        public string ServerUrl {get; set;}
        private Socket socket;
        private BinaryFormatter serializer;
        private CancellationTokenSource cS;
        private List<string> messageBuffer;
        private static readonly object _cursorLock = new object();

        public ChatroomActivity(string username, string serverUrl)
        {
            Username = username;
            ServerUrl = serverUrl;
            messageBuffer = new List<string>();
            socket = null;
            serializer = new BinaryFormatter();
        }
        private void Connect()
        {
            try
            {
                IPHostEntry hostInfo = Dns.GetHostEntry(ServerUrl);
                IPAddress ipAddress = hostInfo.AddressList[0];
                IPEndPoint remote = new IPEndPoint(ipAddress, 6969);

                socket = new Socket(ipAddress.AddressFamily,
                    SocketType.Stream, ProtocolType.Tcp);
                
                socket.Connect(remote);

                NetworkMessage m = new NetworkMessage();
                m.Type = MessageType.Connect;
                m.Username = Username;
                SendMessage(m);
            }
            catch (SocketException e)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Something went wrong: " + e.Message);
                return;
            }

        }
        private void SendMessage(NetworkMessage message)
        {
            using (var ms = new MemoryStream())
            {
                serializer.Serialize(ms, message);
                if (ms.Length < 1024)
                {
                    byte[] bytes = ms.ToArray();
                    socket.Send(bytes);
                }
            }
        }
        public override void Activate()
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Trying to connect to " + ServerUrl + "...");
            Connect();

            Console.ForegroundColor = ConsoleColor.Black;
            Draw();

            cS = new CancellationTokenSource();
            Thread th = new Thread(() => Read(cS.Token));
            th.Start();

            HandleConnection();
        }
        private NetworkMessage BytesToMessage(byte[] bytes)
        {
            using (var ms = new MemoryStream(bytes))
            {
                try
                {
                    NetworkMessage ret = (NetworkMessage)serializer.Deserialize(ms);
                    return ret;
                }
                catch(Exception e)
                {
                    return null;
                }
            }
        }
        private void HandleConnection()
        {
            byte[] bytes = new byte[1024];
            while (true)
            {
                try
                {
                    int bytesRec = socket.Receive(bytes);
                    NetworkMessage m = BytesToMessage(bytes);

                    if (m != null && m.Type == MessageType.Message)
                        messageBuffer.Add(m.Message.Substring(0, m.Message.IndexOf('\0')));
                }
                catch(SocketException e)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Error: " + e.Message);
                    Environment.Exit(-1);
                }
                Draw();
            }
        }
        public override void Draw()
        {
            lock (_cursorLock)
            {
                Console.Clear();

                Console.SetCursorPosition(0, 0);
                Console.Write("Chatting in <");
                Console.ForegroundColor = ConsoleColor.Green;
                Console.Write(ServerUrl);
                Console.ForegroundColor = ConsoleColor.Black;
                Console.Write(">");

                Console.SetCursorPosition(0, 1);
                for (int i = 0; i < Console.WindowWidth; i++)
                    Console.Write("=");
                
                Console.SetCursorPosition(0, Console.WindowHeight-3);
                for (int i = 0; i < Console.WindowWidth; i++)
                    Console.Write("-");

                if (messageBuffer.Count > 0)
                {
                    StringBuilder b = new StringBuilder();
                    Console.SetCursorPosition(0, 2);
                    int index = 0;
                    if (messageBuffer.Count < Console.WindowHeight-6)
                        index = messageBuffer.Count;
                    else
                        index = Console.WindowHeight-6;
                    while (index > 0)
                    {
                        b.Append(messageBuffer[messageBuffer.Count - index] + "\n");
                        index--;
                    }
                    Console.Write(b.ToString());
                }

                Console.SetCursorPosition(0, Console.WindowHeight-2);
                Console.Write(Username + "> ");
            }
        }
        private void Read(CancellationToken token)
        {
            while (!token.IsCancellationRequested)
            {
                lock (_cursorLock)
                    Console.SetCursorPosition(Username.Length+3, Console.WindowHeight-2);

                NetworkMessage m = new NetworkMessage();
                m.Type = MessageType.Message;
                m.Username = Username;
                m.Message = Username + ": " + Console.ReadLine() + '\0';
                try 
                {
                    SendMessage(m);
                }
                catch(SocketException e)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Error: " + e.Message);
                    Environment.Exit(-1);
                }
            }
        }

        public override void HandleEvent(ConsoleEvent e)
        {
            switch (e.Type)
            {
                case EventType.ConsoleResized:
                    Draw();
                    break;
            }
        }
    }
}