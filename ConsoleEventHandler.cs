using System;

namespace Chattr
{
    public interface IConsoleEventHandler
    {
        void HandleEvent(ConsoleEvent consoleEvent);
    }
}