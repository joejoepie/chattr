using System;
using System.Threading;
using System.Text;
using System.Threading.Tasks;

namespace Chattr
{
    public static class ConsoleHelperFunctions
    {
        public static bool interrupt = false;
        public static void WriteCenter(string message, int height)
        {
            Console.SetCursorPosition(Console.WindowWidth/2 - message.Length/2, height);
            Console.Write(message);
        }
    }
}