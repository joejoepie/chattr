using System;
using System.Threading;
using System.Threading.Tasks;

namespace Chattr
{
    public class ConsoleEventListener
    {
        private int consoleBufferWidth;
        private int consoleBufferHeight;
        private IConsoleEventHandler eventHandler;
        private CancellationTokenSource sC;

        public ConsoleEventListener(IConsoleEventHandler e)
        {
            consoleBufferWidth = Console.BufferWidth;
            consoleBufferHeight = Console.BufferHeight;
            sC = new CancellationTokenSource();
            eventHandler = e;
        }
        public void Listen()
        {
            Task t = Task.Run(() => { ListenForEvents(sC.Token); });
        }
        public void Stop()
        {
            sC.Cancel();
        }
        private void ListenForEvents(CancellationToken token)
        {
            while (!token.IsCancellationRequested)
            {
                Thread.Sleep(50);
                if (Console.BufferWidth != consoleBufferWidth || Console.BufferHeight != consoleBufferHeight)
                {
                    consoleBufferWidth = Console.BufferWidth;
                    consoleBufferHeight = Console.BufferHeight;
                    ConsoleEvent e = new ConsoleEvent(EventType.ConsoleResized);
                    eventHandler.HandleEvent(e);
                }
            }
        }
    }
}