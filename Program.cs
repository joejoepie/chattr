﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Chattr
{
    class Program
    {
        private static readonly object _consoleLock = new object();
        static void DisplayWelcomeMessage(ref string username, ref string url)
        {
            Console.Clear();

            // Draw top line
            for (int x = 0; x < Console.BufferWidth; x++)
            {
                Console.Write("#");
            }
            Console.SetCursorPosition(0, 4);
            // Draw bottom line
            for (int x = 0; x < Console.BufferWidth; x++)
            {
                Console.Write("#");
            }
            Console.SetCursorPosition(0, 1);Console.Write("#");
            Console.SetCursorPosition(0, 2);Console.Write("#");
            Console.SetCursorPosition(0, 3);Console.Write("#");
            Console.SetCursorPosition(Console.BufferWidth, 1);Console.Write("#");
            Console.SetCursorPosition(Console.BufferWidth, 2);Console.Write("#");
            Console.SetCursorPosition(Console.BufferWidth, 3);Console.Write("#");

            WriteCenter("Welcome to Chattr!", 2);

            string enterUsernameMessage = "Username:";
            Console.SetCursorPosition(Console.BufferWidth/2 - enterUsernameMessage.Length, Console.BufferHeight/2 - 1);
            Console.Write(enterUsernameMessage);
            Console.SetCursorPosition(Console.CursorLeft + 2, Console.CursorTop);
            username = Console.ReadLine();

            string enterUrlMessage = "Server URL:";
            Console.SetCursorPosition(Console.BufferWidth/2 - enterUrlMessage.Length, Console.BufferHeight/2 + 1);
            Console.Write(enterUrlMessage);
            Console.SetCursorPosition(Console.CursorLeft + 2, Console.CursorTop);
            url = Console.ReadLine();
        }
        static void WriteCenter(string message, int height)
        {
            Console.SetCursorPosition(Console.BufferWidth/2 - message.Length/2, height);
            Console.Write(message);
        }
        static void MainScreen(string username, string url)
        {
            Console.Clear();
            Console.WriteLine(url);

            for (int i = 0; i < Console.BufferWidth; i++)
                Console.Write("-");

            Console.SetCursorPosition(0, Console.BufferHeight-1);
            Console.Write(username + "> ");
            Console.ReadLine();
        }
        static void Print()
        {
            Thread.Sleep(1000);
            Console.Write("lol");
        }
        static void Main(string[] args)
        {
            Chattr chattr = new Chattr();
        }
    }
}
