using System;

namespace Chattr
{
    [Serializable]
    public enum MessageType
    {
        Connect,
        Message,
        File,
        Disconnect
    }

    [Serializable]
    public class NetworkMessage
    {
        public string Username {get; set;}
        public MessageType Type {get; set;}
        public string Message {get; set;}
    }
}